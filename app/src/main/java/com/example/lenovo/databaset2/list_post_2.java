package com.example.lenovo.databaset2;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Adapter.PozoAdapter;
import Database.DatabaseSqlLiteHelper;
import Models.Pozo;

public class list_post_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_post_2);
        setListContent();
    }

    private void setListContent() {

        PozoAdapter adapter = new PozoAdapter(this, R.layout.activity_list_post_item, GetPosts());
        ListView list = (ListView) findViewById(R.id.listPostF);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), null); // activity_list_post_detail.class Tercera activity donde se visualiza el detalle

                String pozo = ((TextView) view.findViewById(R.id.txtVUsuario2)).getText().toString();
                String operador = ((TextView) view.findViewById(R.id.txtVTitle2)).getText().toString();
                String favorito = ((TextView) view.findViewById(R.id.txtVContent2)).getText().toString();


                intent.putExtra("CurrentPostUser", pozo);
                intent.putExtra("CurrentPostTitle", operador);
                intent.putExtra("CurrentPostContent", favorito);


                startActivity(intent);
            }
        });
    }

    private List<Pozo> GetPosts() {

        List<Pozo> posts = new ArrayList<>();
        DatabaseSqlLiteHelper helper = new DatabaseSqlLiteHelper(this, "android", null, 1);
        SQLiteDatabase db = helper.getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM pozos WHERE favorito = 'SI' OR favorito = 'si';", null);

        while (c.moveToNext()) {
            Pozo post = new Pozo();
            post.id = c.getInt(0);
            post.pozo = c.getString(1);
            post.operador = c.getString(2);
            post.favorito = c.getString(3);

            posts.add(post);
        }
        return posts;
    }
        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_list_post_2, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }

