package com.example.lenovo.databaset2;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Database.DatabaseSqlLiteHelper;
import Models.Pozo;

public class form_post extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_post);

        Button btn = (Button) findViewById(R.id.btnGuardar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GUARDAR EN BASE DE DATOS
                DatabaseSqlLiteHelper helper = new DatabaseSqlLiteHelper(getApplicationContext(), "android", null, 1);

                SQLiteDatabase database = helper.getWritableDatabase();
                ContentValues valores = new ContentValues();

                Pozo post = new Pozo();
                post.pozo = ((EditText) findViewById(R.id.txtUsuario)).getText().toString();
                post.operador = ((EditText) findViewById(R.id.txtTitle)).getText().toString();
                post.favorito = ((EditText) findViewById(R.id.txtContent)).getText().toString();


                valores.put("pozo", ((EditText) findViewById(R.id.txtUsuario)).getText().toString());
                valores.put("operador", ((EditText) findViewById(R.id.txtTitle)).getText().toString());
                valores.put("favorito", ((EditText) findViewById(R.id.txtContent)).getText().toString());

                database.insert("pozos", null, valores);

                Toast.makeText(getApplicationContext(), "Se guardo correctamente", Toast.LENGTH_SHORT).show();

                database.close();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
