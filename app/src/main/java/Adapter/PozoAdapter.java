package Adapter;


import com.example.lenovo.databaset2.R;

import Models.Pozo;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by LENOVO on 13/11/2015.
 */
public class PozoAdapter extends ArrayAdapter<Pozo>{

    private int resource;

    public PozoAdapter(Context context, int resource, List<Pozo> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null)

            convertView = inflater.inflate(this.resource, parent, false);

        Pozo item = getItem(position);

        ((TextView)convertView.findViewById(R.id.txtVTitle)).setText(item.pozo);
        ((TextView)convertView.findViewById(R.id.txtVUsuario)).setText(item.operador);
        ((TextView)convertView.findViewById(R.id.txtVContent)).setText(item.favorito);

        //Seteo de la estrella de acuerdo a si es favorito o no..

        if(item.favorito.contains("SI")||(item.favorito.contains("si"))){
            ((ImageView)convertView.findViewById(R.id.imgV)).setImageResource(R.drawable.onfavorite);
        }
        else if (item.favorito.contains("NO")||(item.favorito.contains("no"))){
            ((ImageView)convertView.findViewById(R.id.imgV)).setImageResource(R.drawable.favorite);
        }

        return convertView;
    }
}
